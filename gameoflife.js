let boxLength = 10;
let boxColor = 255;
let strokeColor = 50;
let columns;
let rows;
let currentBoard;
let nextBoard;
let toggling;
let deathColor = 152;
let gGG = 
`........................O
......................O.O
............OO......OO............OO
...........O...O....OO............OO
OO........O.....O...OO
OO........O...O.OO....O.O
..........O.....O.......O
...........O...O
............OO`


function ranColors() {
    boxColor = 'rgb(' + Math.round(Math.random() * 255) + ',' + Math.round(Math.random() * 255) + ',' + Math.round(Math.random() * 255) + ')';
    console.log(boxColor);
};

function resetColor(){
    boxColor = 255
}
function onoff(state) {
    if (state == "0") {
        return init();
    } else {
        return init2();
    }
}

// document.querySelector('#gliderGun').addEventListener("click", function(){
//     gosperGliderGun();
// })

document.querySelector('#minus').addEventListener("click", function () {
    onoff(0);
});

document.querySelector('#plus').addEventListener("click", function () {
    onoff(1);
});

function setup() {
    const canvas = createCanvas(1125, 650 - 100); // creates window width and height (minus 100 px to give some room)
    canvas.parent(document.querySelector('#canvas')); // insert canvas to the ID: canvas
    columns = floor(width / boxLength); // floor is the nearest round number
    rows = floor(height / boxLength); // rows equal to height divided by boxLength
    // toggle();
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = []; // initialise the currentBoard as an array of array
        nextBoard[i] = [];
    }
    init();
}

function init() {
    for (let i = 0; i < columns; i++) { // runs through all the columns until 0
        for (let j = 0; j < rows; j++) { // runs through all the rows until 0
            currentBoard[i][j] = 0; // random() > 0.8 ? 1 : 0;
            // current board to be set randomly, if random number is greater than 0.8, get back 1
            nextBoard[i][j] = 0; // next board is all 0
        }
    }
}

function init2() {
    for (let i = 0; i < columns; i++) { // runs through all the columns until 0
        for (let j = 0; j < rows; j++) { // runs through all the rows until 0
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            // current board to be set randomly, if random number is greater than 0.8, get back 1
            nextBoard[i][j] = 0; // next board is all 0
        }
    }
}

// function gosperGliderGun(){
//     let goseperGliderGunArray = arrConverse(gGG)
//     for(let i = 0; i < goseperGliderGunArray.length && i < columns; i++){
//         for(let j = 0; j < goseperGliderGunArray[i].length && j < rows; j++){
//             currentBoard[j][i] = goseperGliderGunArray[i][j]
//             nextBoard[i][j] = 0
//         }
//     }
//     fill(boxColor);
// }


function draw() {
    background(28); 
    generate();
    frameRate(parseInt(fps.value));
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) { // check if box has life (i.e. 1)
                fill(boxColor);
            } else {
                fill(28);
            }
            stroke(strokeColor);
            rect(i * boxLength, j * boxLength, boxLength, boxLength, 20, 55, 55, 20);
        }
    }
}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) { // runs through the x-axis from -1 to 1
                for (let j of [-1, 0, 1]) { // runs through the y-axis from -1 to 1
                    if (i === 0 && j === 0) { // 0,0 is the centre box, the box with life
                        // the cell itself is not its own neighbor
                        continue; // continue skips the 0,0 cell
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows]; // % return the remainder of two operands
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) { // if neightbour is less than 2, dies
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) { //if neighbour greater than 3, dies
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) { // if neighbour equal 3, new life
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > boxLength * columns || mouseY > boxLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / boxLength);
    const y = Math.floor(mouseY / boxLength);
    currentBoard[x][y] = 1;
    console.log(boxColor);
    fill(boxColor);
    stroke(strokeColor);
    rect(x * boxLength, y * boxLength, boxLength, boxLength, 20, 55, 55, 20);
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop(); //p5.js function, it stops the draw() function from executing while mouse is clicked
    mouseDragged(); // executes mouseDragged alongside mousePressed
}

/**
 * When mouse is released
 */
function mouseReleased() {
    loop(); //resumes draw() function
}

document.querySelector('#reset-game')
    .addEventListener('click', function () {
        init();
    });

document.querySelector('#start')
    .addEventListener('click', function () {
        loop();
    });

document.querySelector('#stop')
    .addEventListener('click', function () {
        noLoop();
    });


// var cell = document.getElementById("cell");

// function changeColor() {
//     cell.style.backgroundColor = 255;
// }

// setInterval(changeColor, 1000);